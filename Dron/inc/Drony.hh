#ifndef DRONY_HH
#define DRONY_HH

#include "Wektor.hh"
#include "Macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include "Interfejsy.hh"
#include "Czesci_dron.hh"
#include <iostream>
#include <array>

class Dron: public Interfejs_rysowanie, protected Uklad_W, public Interfejs_Dron, public Interfejs_elem_kraj {
    Prostopadloscian korpus;
    std::array<Graniastoslup6, 4> wierniki;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik) const override;
    void przekrec_wirniki(drawNS::Draw3DAPI* rysownik) override;
    void lec_przod(double ile,drawNS::Draw3DAPI* rysownik) override;
    void lec_gora(double ile,drawNS::Draw3DAPI* rysownik) override;
    void obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik)override;
    bool czy_nad(Interfejs_Dron *D)override{return true;};
    bool czy_ladowac(Interfejs_Dron* D, double & wys)override{return false;};
    Wektor<3> get_srodek() override {return srodek;};
    //konstruktor
    Dron(Wektor<3> Sro, Macierz_Rot<3> Orient):
    Uklad_W(Sro, Orient, nullptr),
    korpus(Wektor<3>({0,0,0}), Macierz_Rot<3>(), this, 2, 3, 1), 
    wierniki({Graniastoslup6(Wektor<3>({1,1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor<3>({1,-1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor<3>({-1,-1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor<3>({-1,1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8)}) {};

};

/*******************************************************************************/
class Dron2: public Interfejs_rysowanie, protected Uklad_W, public Interfejs_Dron, public Interfejs_elem_kraj {
    Prostopadloscian korpus;
    std::array<Graniastoslup6, 3> wierniki;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik) const override;
    void przekrec_wirniki(drawNS::Draw3DAPI* rysownik) override;
    void lec_przod(double ile,drawNS::Draw3DAPI* rysownik) override;
    void lec_gora(double ile,drawNS::Draw3DAPI* rysownik) override;
    void obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik)override;
    bool czy_nad(Interfejs_Dron *D)override{return true;};
    bool czy_ladowac(Interfejs_Dron* D, double & wys)override{return false;};
    Wektor<3> get_srodek() override {return srodek;};
    //konstruktor
    Dron2(Wektor<3> Sro, Macierz_Rot<3> Orient):
    Uklad_W(Sro, Orient, nullptr),
    korpus(Wektor<3>({0,0,0}), Macierz_Rot<3>(), this, 2, 3, 1), 
    wierniki({Graniastoslup6(Wektor<3>({1,1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor<3>({-1,1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor<3>({0,-1.5,0.7}),Macierz_Rot<3>(),this,0.6,1)}) {};

};
/*******************************************************************************/
class Dron3: public Interfejs_rysowanie, protected Uklad_W, public Interfejs_Dron, public Interfejs_elem_kraj {
    Prostopadloscian korpus;
    std::array<Graniastoslup6, 2> wierniki;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik) const override;
    void przekrec_wirniki(drawNS::Draw3DAPI* rysownik) override;
    void lec_przod(double ile,drawNS::Draw3DAPI* rysownik) override;
    void lec_gora(double ile,drawNS::Draw3DAPI* rysownik) override;
    void obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik)override;
    bool czy_nad(Interfejs_Dron *D)override{return true;};
    bool czy_ladowac(Interfejs_Dron* D, double & wys)override{return false;};
    Wektor<3> get_srodek() override {return srodek;};
    //konstruktor
    Dron3(Wektor<3> Sro, Macierz_Rot<3> Orient):
    Uklad_W(Sro, Orient, nullptr),
    korpus(Wektor<3>({0,0,0}), Macierz_Rot<3>(), this, 2, 4, 1), 
    wierniki({Graniastoslup6(Wektor<3>({0,1.7,0.7}),Macierz_Rot<3>(),this,0.8,1.2),
    Graniastoslup6(Wektor<3>({0,-1.7,0.7}),Macierz_Rot<3>(),this,0.8,1.2)}) {};
};



#endif