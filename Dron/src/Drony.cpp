#include "Drony.hh"

void Dron::rysuj(drawNS::Draw3DAPI* rysownik){
    korpus.rysuj(rysownik);
    wierniki[0].rysuj(rysownik);
    wierniki[1].rysuj(rysownik);
    wierniki[2].rysuj(rysownik);
    wierniki[3].rysuj(rysownik);
}

void Dron::usun_rys(drawNS::Draw3DAPI* rysownik) const{
    korpus.usun_rys(rysownik);
    wierniki[0].usun_rys(rysownik);
    wierniki[1].usun_rys(rysownik);
    wierniki[2].usun_rys(rysownik);
    wierniki[3].usun_rys(rysownik);

}

void Dron::przekrec_wirniki(drawNS::Draw3DAPI* rysownik){
    Macierz_Rot<3> mac_obr(15,Wektor<3>({0,0,1}));
    usun_rys(rysownik);
    wierniki[0].Obrot(mac_obr);
    wierniki[1].Obrot(mac_obr);
    wierniki[2].Obrot(mac_obr);
    wierniki[3].Obrot(mac_obr);
    rysuj(rysownik);
}

void Dron::lec_przod(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + orientacja*Wektor<3>({0,ile,0});
    rysuj(rysownik);
}

void Dron::lec_gora(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + Wektor<3>({0,0,ile});
    rysuj(rysownik);
}

void Dron::obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    Macierz_Rot<3> Mac_obr(kat_stopnie, Wektor<3>({0,0,1}));
    orientacja = Mac_obr*orientacja;
    rysuj(rysownik);
}
/*********************************************************************/
void Dron2::rysuj(drawNS::Draw3DAPI* rysownik){
    korpus.rysuj(rysownik);
    wierniki[0].rysuj(rysownik);
    wierniki[1].rysuj(rysownik);
    wierniki[2].rysuj(rysownik);
}

void Dron2::usun_rys(drawNS::Draw3DAPI* rysownik) const{
    korpus.usun_rys(rysownik);
    wierniki[0].usun_rys(rysownik);
    wierniki[1].usun_rys(rysownik);
    wierniki[2].usun_rys(rysownik);

}

void Dron2::przekrec_wirniki(drawNS::Draw3DAPI* rysownik){
    Macierz_Rot<3> mac_obr(15,Wektor<3>({0,0,1}));
    usun_rys(rysownik);
    wierniki[0].Obrot(mac_obr);
    wierniki[1].Obrot(mac_obr);
    wierniki[2].Obrot(mac_obr);

    rysuj(rysownik);
}

void Dron2::lec_przod(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + orientacja*Wektor<3>({0,ile,0});
    rysuj(rysownik);
}

void Dron2::lec_gora(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + Wektor<3>({0,0,ile});
    rysuj(rysownik);
}

void Dron2::obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    Macierz_Rot<3> Mac_obr(kat_stopnie, Wektor<3>({0,0,1}));
    orientacja = Mac_obr*orientacja;
    rysuj(rysownik);
}
/*******************************************************************************/

void Dron3::rysuj(drawNS::Draw3DAPI* rysownik){
    korpus.rysuj(rysownik);
    wierniki[0].rysuj(rysownik);
    wierniki[1].rysuj(rysownik);
}

void Dron3::usun_rys(drawNS::Draw3DAPI* rysownik) const{
    korpus.usun_rys(rysownik);
    wierniki[0].usun_rys(rysownik);
    wierniki[1].usun_rys(rysownik);

}

void Dron3::przekrec_wirniki(drawNS::Draw3DAPI* rysownik){
    Macierz_Rot<3> mac_obr(15,Wektor<3>({0,0,1}));
    usun_rys(rysownik);
    wierniki[0].Obrot(mac_obr);
    wierniki[1].Obrot(mac_obr);
    rysuj(rysownik);
}

void Dron3::lec_przod(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + orientacja*Wektor<3>({0,ile,0});
    rysuj(rysownik);
}

void Dron3::lec_gora(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + Wektor<3>({0,0,ile});
    rysuj(rysownik);
}

void Dron3::obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    Macierz_Rot<3> Mac_obr(kat_stopnie, Wektor<3>({0,0,1}));
    orientacja = Mac_obr*orientacja;
    rysuj(rysownik);
}