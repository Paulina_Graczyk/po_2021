#include <iostream>
#include <cstring>
#include <cassert>
#include "BazaTestu.hh"


/************************************************************
 * Metoda otwierajaca plik o danej nazwie			         			*
 * Argumenty:									                        			*
 *    nazwa - nazwa pliku do otwarcia               		    *
 * Zwraca:										                        			*
 *    true jesli ok, false jesli nie udalo sie otworzyc			*
 ***********************************************************/
  bool BazaTestu::otworz_plik (std::string nazwa){
    strm_plik.open(nazwa, std::fstream::in);
    if(!strm_plik.good()){
      std::cerr << "Nie mozna otworzyc pliku" << nazwa << std::endl;
      return false;
    } else {
      return true;
    }
  }

/************************************************************
 * Metoda pobierajaca pytanie z pliku		              			*
 * Argumenty:									                        			*
 *    Wyr - wyrazenie do ktorego chcemy wczytac      		    *
 * Zwraca:										                        			*
 *    true jesli ok, false jesli koniec pliku	            	*
 ***********************************************************/
  bool BazaTestu::pobierz_pytanie(WyrazenieZesp & Wyr){
    strm_plik >> Wyr;
    while (strm_plik.fail()){  
      if (strm_plik.eof()){   //jesli blad to zobacz czy koniec pliku
        strm_plik.close();
        return false;
      } else {                //jesli nie koniec pliku to oznacza wyrazenie jest nie poprawne
      std::cerr << "Blad odczytu wyrazenia, zostalo ono pominiete" << std::endl << std::endl;
      strm_plik.clear();
      strm_plik.ignore(10000, '\n');    //zignoruj i wczytaj nowe
      strm_plik >> Wyr;
      }
    } return true;    //jesli brak bledu zwroc true
  }
