#include "LZespolona.hh"


/************************************************************
 * Metoda inicjuje liczbe zespolona							*
 * Argumenty:												*
 *    Re - czesc rzeczywista liczby do inicjacji		    *
 * 	  Im - czesc urojona liczby do incjacji					*
 * Zwraca:													*
 *    zainicjowana liczbe zespolona							*
 ***********************************************************/
LZespolona LZespolona::inicjuj_licz(double Re, double Im){   
	LZespolona Liczba(Re, Im);
	return Liczba;
}

/************************************************************
 * Metoda realizuje modul liczby zespolonej					*
 * Zwraca:													*
 *    Modul z liczby zespolonej								*
 ***********************************************************/
double LZespolona::modul() const {
	return sqrt(pow(re,2) + pow(im,2));
}

/************************************************************
 * Metoda realizuje sprzezenie liczby zespolonej			*
 * Zwraca:													*
 *    Sprzezenie liczby zespolonej							*
 ***********************************************************/
LZespolona LZespolona::sprzezenie () const {
	LZespolona Wynik;
	Wynik.re = re;
	Wynik.im = -im;
	return Wynik;
}


/************************************************************
 * Wypisuje liczbe zespolona na strumien wyjsciowy			*
 * Argumenty:												*
 *    s - strumien											*
 *    Liczba - wyswietlana liczba							*
 * Zwraca:													*
 *    Strumien z pelnym zapisem liczby zespolonej			*
 ***********************************************************/
std::ostream & operator << (std::ostream & s, const LZespolona & Liczba){
	s << "(" << Liczba.get_re() << std::showpos << Liczba.get_im() << std::noshowpos << "i)";
	return s;
}

/************************************************************
 * Wczytuje liczbe zespolona ze strumienia wejsciowego		*
 * Argumenty:												*
 *    s - strumien											*
 *    Liczba - zapisywana liczba							*
 * Zwraca:													*
 *    Strumien z pelnym zapisem liczby zespolonej 			*
 ***********************************************************/
std::istream & operator >> (std::istream & s, LZespolona & Liczba){
	char temp;
	double temp_liczba;
	s >> temp;
	if(temp != '('){
		s.setstate(std::ios::failbit);
	}
	s >> temp_liczba;
	Liczba.set_re(temp_liczba);
	s >> temp_liczba;
	Liczba.set_im(temp_liczba);
	s >> temp;
	if(temp != 'i'){
		s.setstate(std::ios::failbit);
	}
	s >> temp;
		if(temp != ')'){
		s.setstate(std::ios::failbit);
	}
	
	return s;
}

/************************************************************
 * Realizuje dodanie dwoch liczb zespolonych.				*
 * Argumenty:												*
 *    Skl2 - drugi skladnik	sumy							*
 * Zwraca:													*
 *    Sume dwoch liczb zespolonych							*
 ***********************************************************/
LZespolona LZespolona::operator+(const LZespolona & Skl2) const {
  LZespolona  Wynik;

  Wynik.re = re + Skl2.re;
  Wynik.im = im + Skl2.im;
  return Wynik;
}

/************************************************************
 * Realizuje odejmowanie dwoch liczb zespolonych.			*
 * Argumenty:												*
 *    Skl2 - drugi skladnik	roznicy							*
 * Zwraca:													*
 *    Roznice dwoch liczb zespolonych						*
 ***********************************************************/
LZespolona LZespolona::operator-(const LZespolona & Skl2) const{
	LZespolona Wynik;
	
	Wynik.re = re - Skl2.re;
	Wynik.im = im - Skl2.im;
	return Wynik;
}

/************************************************************
 * Realizuje mnozenie dwoch liczb zespolonych.				*
 * Argumenty:												*
 *    Skl2 - drugi skladnik	iloczynu						*
 * Zwraca:													*
 *    Iloczyn dwoch liczb zespolonych						*
 ***********************************************************/
LZespolona LZespolona::operator*(const LZespolona & Skl2) const{
	LZespolona Wynik;

	Wynik.re = (re * Skl2.re) - (im * Skl2.im);
	Wynik.im = (re * Skl2.im) + (im * Skl2.re);
	return Wynik;
}

/************************************************************
 * Realizuje dzielenie dwoch liczb zespolonych.				*
 * Argumenty:												*
 *    Skl2 - drugi skladnik	ilorazu							*
 * Zwraca:													*
 *    Iloraz dwoch liczb zespolonych						*
 ***********************************************************/
LZespolona LZespolona::operator/(const LZespolona & Skl2) const {
	LZespolona Wynik;

	Wynik = (*this)*Skl2.sprzezenie()/pow(Skl2.modul(),2);
	return Wynik;
}

/************************************************************
 * Realizuje dzielenie liczby zespolonej przez calkowita	*
 * Argumenty:												*
 *    Skl2 - liczba całkowita przez ktora dzielimy			*
 * Zwraca:													*
 *    Iloraz dwoch l. zespolonej i calkowitej				*
 * Warunek:													*
 *	  Skl2 musi byc rozny od 0								*
 ************************************************************/
LZespolona LZespolona::operator/(double Skl2) const {
	LZespolona Wynik;
	if (Skl2 == 0.0){
		std::cerr << "blad dzielenia przez 0" << std::endl;
		exit(1);
	}

	Wynik.re = re / Skl2;
	Wynik.im = im / Skl2;
	return Wynik;
}

/************************************************************
 * Realizuje porownanie dwoch liczb zespolonych.			*
 * Argumenty:												*
 *    Skl2 - drugi skladnik									*
 * Zwraca:													*
 *    true jesli takie same, false jesli nie				*
 *******************************************************-****/
bool LZespolona::operator==(const LZespolona & Skl2) const{
	const double EPSILON = 1E-14;
	if (fabs(re - Skl2.re) > EPSILON) {
		return false;
	}
	if (fabs(im - Skl2.im) > EPSILON) {
		return false;
	}
	return true;
}

/************************************************************
 * Realizuje porownanie dwoch liczb zespolonych.			*
 * Argumenty:												*
 *    Skl2 - drugi skladnik									*
 * Zwraca:													*
 *   true jesli rozne, false jesli nie						*
 ***********************************************************/
bool LZespolona::operator!=(const LZespolona & Skl2) const{
	return !(*this==Skl2); 
}