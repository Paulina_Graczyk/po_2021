#include <iostream>
#include "BazaTestu.hh"
#include "Statystyka.hh"

int main(int argc, char **argv)
{
  if (argc < 2) {
    std::cout << std::endl;
    std::cout << " Brak opcji okreslajacej rodzaj testu." << std::endl;
    std::cout << " Dopuszczalne nazwy to:  latwy, trudny." << std::endl;
    std::cout << std::endl;
    return 1;
  }

  BazaTestu   BazaT;
  /*sprawdzenie czy udalo sie otworzyc plik*/
  if(!BazaT.otworz_plik(argv[1])){
    exit(1);
  }
  
  WyrazenieZesp   WyrZ_PytanieTestowe;
  LZespolona  odpowiedz;
  int szansa; 
  Statystyki stat_1;
  
  std::cout << std::endl;
  std::cout << " Start testu arytmetyki zespolonej: " << argv[1] << std::endl;
  std::cout << std::endl;

    while(BazaT.pobierz_pytanie(WyrZ_PytanieTestowe)){
    szansa=1;
    std::cout << "Oblicz: " << WyrZ_PytanieTestowe << std::endl;
    std::cout << "Twoja odpowiedz: ";

    /*Wczytanie i sprawdzenie poprawnosci technicznej odpowiedzi*/
    while(!(std::cin >> odpowiedz) && (szansa<3)){ 
      std::cout << std::endl;
      std::cout << "Niepoprawnie wpisana odpowiedz" << std::endl;
      std::cout << "Poprawna forma to np.: (1+1i)" << std::endl;
      std::cout << "Sprobuj jeszcze raz: ";
      std::cin.clear();
      std::cin.ignore(10000, '\n');
      szansa++;
    }

    if(szansa>=3){ // Jesli przekroczona ilosc 3 prob pytanie niezaliczone
      std::cout << std::endl;
      std::cout << "Przekroczona ilosc szans" << std::endl;
      std::cout << std::endl;
      std::cin.clear();
      std::cin.ignore(10000, '\n');
      stat_1.dodaj_niepoprawna();
    } else { 
    /*jesli poprawna technicznie i nie przekroczona ilosc szans to sprawdzanie poprawnosci merytorycznej*/
      if(WyrZ_PytanieTestowe.oblicz()==odpowiedz){
        std::cout << std::endl;
        std::cout << "Poprawna odpowiedz, gratulacje!" << std::endl;
        std::cout << std::endl;
        stat_1.dodaj_poprawna();
      } else {
        std::cout << std::endl;
        std::cout << "Bledna odpowiedz :(" << std::endl;
        std::cout << "Poprawna odpowiedz to: " << WyrZ_PytanieTestowe.oblicz() << std::endl;
        std::cout << std::endl;
        stat_1.dodaj_niepoprawna();
      }
    }
  }
  /* Koniec testu wypisanie wynikow*/
  std::cout << std::endl;
  std::cout << " Koniec testu" << std::endl;
  std::cout << std::endl;
  stat_1.wypisz_wynik();

}
