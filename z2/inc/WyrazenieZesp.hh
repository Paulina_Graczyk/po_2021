#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH



#include "LZespolona.hh"


/*
 * Modeluje zbior operatorow arytmetycznych.
 */
enum Operator { Op_Dodaj, Op_Odejmij, Op_Mnoz, Op_Dziel };


class WyrazenieZesp {
private:
	LZespolona   Arg1;   // Pierwszy argument wyrazenia arytmetycznego
	Operator     Op;     // Opertor wyrazenia arytmetycznego
	LZespolona   Arg2;   // Drugi argument wyrazenia arytmetycznego
public:
	WyrazenieZesp inicjuj_wyr(const LZespolona & Liczba1, const LZespolona & Liczba2, Operator O);
	void wyswietl() const; 
	LZespolona oblicz() const;
	//konstruktor:
	WyrazenieZesp(const LZespolona& Liczba1,  Operator O, const LZespolona& Liczba2) : Arg1(Liczba1), Op(O), Arg2(Liczba2) {}
	WyrazenieZesp(): Arg1(0.0), Op(), Arg2(0.0) {}
	//pobieranie i ustwianie wartosci Arg1, Arg2, Op
	LZespolona get_Arg1() const { return Arg1; };
	LZespolona get_Arg2() const { return Arg2; };
	Operator get_Op() const { return Op; };
	void set_Arg1(LZespolona Liczba) { Arg1 = Liczba; };
	void set_Arg2(LZespolona Liczba) { Arg2 = Liczba; };
	void set_Op(Operator O) { Op = O; };

};

std::ostream & operator << (std::ostream & s, const WyrazenieZesp & Wyr);
std::ostream & operator << (std::ostream & s, const Operator & O);
std::istream & operator >> (std::istream & s,  WyrazenieZesp & Wyr);
std::istream & operator >> (std::istream & s,  Operator & O);

#endif
