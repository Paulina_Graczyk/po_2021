#ifndef  STATYSTYKA_HH
#define  STATYSTYKA_HH

class Statystyki {
private:
    unsigned int L_poprawnych; //pole zawiera informacje o ilosci poprawnych odp
    unsigned int L_niepoprawnych;   //pole zawiera informacje o ilosci niepoprawnych odp
public:
    Statystyki inicjuj_stat(unsigned int Liczba_popraw, unsigned int Liczba_niepopraw);
    unsigned int oblicz_procent()const;
    void wypisz_wynik()const;
    void dodaj_poprawna();
    void dodaj_niepoprawna();
    //konstruktory:
    Statystyki(unsigned int Liczba_popraw, unsigned int Liczba_niepopraw) : L_poprawnych(Liczba_popraw), L_niepoprawnych(Liczba_niepopraw) {}
    Statystyki(): L_poprawnych(0), L_niepoprawnych(0) {}
    //pobieranie i ustawianie warosci L_poprawnych i L_niepoprawnych
    unsigned int get_L_popraw()const {return L_poprawnych;};
    unsigned int get_L_niepopraw()const { return L_niepoprawnych; };
    void set_L_popraw(unsigned int L_Pop) { L_poprawnych = L_Pop; };
    void set_L_niepopraw(unsigned int L_NPop) { L_niepoprawnych = L_NPop; };
};


#endif
