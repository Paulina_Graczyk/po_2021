#ifndef BAZATESTU_HH
#define BAZATESTU_HH


#include "WyrazenieZesp.hh"
#include <fstream>

class BazaTestu {
private:
  std::fstream strm_plik; //strumien danych z pliku
public:
  bool otworz_plik (std::string nazwa);
  bool pobierz_pytanie(WyrazenieZesp & Wyr);
  //konstruktory
  BazaTestu(std::string nazwa) {otworz_plik(nazwa);};
  BazaTestu() {};
};

#endif
