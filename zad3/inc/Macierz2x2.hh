#ifndef MACIERZ2X2_HH
#define MACIERZ2X2_HH

#include"Wektor2D.hh"
#include <iostream>
#include <array>

/*definiuje macierz obrotu 2x2 */ 
class Macierz2x2 {
  std::array<Wektor2D,2> wiersz;
  public:
  Macierz2x2 transpozycja()const;  
  Wektor2D operator*(const Wektor2D & skl2) const;
  Macierz2x2 operator*(const Macierz2x2 & skl2) const;
  //konstruktory:
  Macierz2x2();//identycznosciowa
  Macierz2x2(double kat_stopnie);
  //get:
  const Wektor2D & operator[](int i) const{
    if(i<0||i>1){ std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wiersz[i];};
};

std::ostream& operator << (std::ostream &Strm, const Macierz2x2 &Mac);

#endif
