#ifndef PROSTOKAT_HH
#define PROSTOKAT_HH

#include"Wektor2D.hh"
#include "Macierz2x2.hh"
#include "Dr2D_gnuplot_api.hh"
#include <iostream>
#include <array>



class Prostokat {
  std::array<Wektor2D,4> wierzcholek;
  int id;
public:
  void obrot(double kat_stopnie);
  void przesuniecie (Wektor2D Wek);
  bool czyProstokat();
  void rysuj(drawNS::Draw2DAPI* rysownik);
  void usun_rys(drawNS::Draw2DAPI* rysownik);
  //konstruktory:
  Prostokat (Wektor2D LG, Wektor2D PG, Wektor2D PD, Wektor2D LD);
  //get:
  const Wektor2D & operator[] (int i) const{
    if(i<0||i>4){std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wierzcholek[i];}

};

std::ostream& operator << ( std::ostream &Strm,const Prostokat&Pr);
drawNS::Point2D konwertuj(Wektor2D W);

#endif
