#include <iostream>
#include <iomanip>
#include <fstream>
#include "Wektor2D.hh"
#include "Macierz2x2.hh"
#include "Prostokat.hh"

/************************************************************
 * Funkcja wyswietla menu na standardowe wyjscie            *
 ***********************************************************/
void wyswietl_menu(){
    std::cout << "Dostepne opcje: " << std::endl;
    std::cout << "**************************************" << std::endl;
    std::cout << "o - obrot prostokata o kat" << std::endl;
    std::cout << "p - przesuniecie prostokata o wektor" << std::endl;
    std::cout << "w - wyswietl wspolrzedne wierzcholkow" << std::endl;
    std::cout << "m - wyswietl menu" << std::endl;
    std::cout << "k - zakoncz program  " << std::endl;
}

int main(){ 
char wybor;
double kat;
int obroty;
Wektor2D wek_przes;
/*Wierzcholki prostokata*/
Wektor2D wek1(0.0,3.0);
Wektor2D wek2(5.0,3.0);
Wektor2D wek3(5.0,0.0);
Wektor2D wek4(0.0,0.0);
Prostokat Prostokat_1(wek1,wek2,wek3,wek4);
drawNS::Draw2DAPI * rysownik = new drawNS::APIGnuPlot2D(-10,10,-10,10,0);

/*Ustawienie ilosci wyswietlanych miejsc po przecinku*/
std::cout << std::setprecision(15)<<std::fixed;

std::cout << "Twoj protokat to:" << std::endl;
std::cout << Prostokat_1;
Prostokat_1.rysuj(rysownik);
std::cout << std::endl;

wyswietl_menu();
while (wybor!= 'k'){
    std::cout << "Twoj wybor: (m - menu) > ";
    std::cin >> wybor;
    switch(wybor){
    case 'o' : //obrot o kat
        Prostokat_1.usun_rys(rysownik); //zmaz poprzednio narysowany prostokat
        std::cin.clear();
        std::cout << "Podaj kat w stopniach: ";
        std::cin >> kat;
        std::cout << "Ile razy chcesz obrocic? > ";
        std::cin >> obroty;
        for(int i=0; i<obroty; i++){
            Prostokat_1.obrot(kat);
        }
        Prostokat_1.rysuj(rysownik);
        if(Prostokat_1.czyProstokat()){
            std::cout << "Boki i katy nadal sie zgadzaja :)" << std::endl;
        } else {
            std::cout << "Boki i katy przestaly sie zgadzac :(" <<std::endl;
            std::cout << "Pierwsza para bokow:  " <<std::endl;
            std::cout << (Prostokat_1[0]- Prostokat_1[1]).dlugosc() << "    " << (Prostokat_1[2]-Prostokat_1[3]).dlugosc() << std::endl;
            std::cout << "Druda para bokow: " <<std::endl;
            std::cout << (Prostokat_1[1]- Prostokat_1[2]).dlugosc() << "    " << (Prostokat_1[3]-Prostokat_1[0]).dlugosc() << std::endl;
        }
        break;
    case 'p' :  //przesuniecie o wektor
        Prostokat_1.usun_rys(rysownik); //zmaz poprzednio narysowany prostokat
        std::cout << "Podaj wektor przesuniecia: (poprawna forma to (1,2) ) > ";
        while(!(std::cin >> wek_przes)){    //sprawdzenie poprawnosci wpisania wektoru
            std::cerr << "Nie poprawnie wpisany wektor!"<< std::endl;
            std::cout << "Sprobuj jeszcze raz: ";
            std::cin.clear();
            std::cin.ignore(10000, '\n');
        }
        Prostokat_1.przesuniecie(wek_przes);
        Prostokat_1.rysuj(rysownik);
        break;
    case 'w' :  //wyswietlenie wierzcholkow
        std::cout << Prostokat_1;
        break;
    case 'm' :  //wyswielt menu
        wyswietl_menu();
        break;
    case 'k' : //zakoncz program
        break;
    default : 
        std::cerr << "Brak takiej opcji!" << std::endl;
        std::cout << "Sproboj jeszcze raz: " << std::endl;
    }
}

delete rysownik;
exit(0);    // zakoncz program pomyslnie
}

