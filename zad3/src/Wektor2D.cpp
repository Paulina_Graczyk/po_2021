#include "Wektor2D.hh"

/************************************************************
 * Realizuje dodanie wektorow                               *
 * Argumenty:                                               *
 *      skl2 - drugi skladnik sumy         					*
 * Zwraca:													*
 *    	Sume dwoch wektorow     							*
 ***********************************************************/
Wektor2D Wektor2D::operator+(const Wektor2D & skl2) const{
    Wektor2D wektor;
    wektor.wsp[0] = wsp[0] + skl2.wsp[0];
    wektor.wsp[1] = wsp[1] + skl2.wsp[1];
    return wektor;
}

/************************************************************
 * Realizuje odejmowanie wektorow                           *
 * Argumenty:                                               *
 *      skl2 - drugi skladnik roznicy     					*
 * Zwraca:													*
 *    	Roznice dwoch wektorow     							*
 ***********************************************************/
Wektor2D Wektor2D::operator-(const Wektor2D & skl2) const{
    Wektor2D wektor;
    wektor.wsp[0] = wsp[0] - skl2.wsp[0];
    wektor.wsp[1] = wsp[1] - skl2.wsp[1];
    return wektor;
}

/************************************************************
 * Realizuje mnozenie wektora razy liczbe                   *
 * Argumenty:                                               *
 *      skl2 - liczba razy ktora mnozymy wektor 			*
 * Zwraca:													*
 *    	Iloczyn wektora i liczby  							*
 ***********************************************************/
Wektor2D Wektor2D::operator*(double skl2) const{
    Wektor2D wektor;
    wektor.wsp[0] = wsp[0] * skl2;
    wektor.wsp[1] = wsp[1] * skl2;
    return wektor;
}

/************************************************************
 * Realizuje iloczyn skalarny wektorow                      *
 * Argumenty:                                               *
 *      skl2 - drugi skladnik iloczynu     					*
 * Zwraca:													*
 *    	Iloczyn skalarny dwoch wektorow     				*
 ***********************************************************/
double Wektor2D::operator*(const Wektor2D & skl2) const{
    return (wsp[0]*skl2.wsp[0]+wsp[1]*skl2.wsp[1]);
}

/************************************************************
 * Oblicza dlugosc wektora                                  *
 * Zwraca:													*
 *    	Dlugosc wektora         							*
 ***********************************************************/
double Wektor2D::dlugosc() const{
    return (sqrt(pow(wsp[0],2)+pow(wsp[1],2)));
}

/************************************************************
 * Wczytuje wektor ze strumienia wejsciowego        		*
 * Argumenty:												*
 *    strm - strumien										*
 *    wek - wczytywany wektor      							*
 * Zwraca:													*
 *    Strumien z wczytanym wektorem              			*
 ***********************************************************/
std::istream& operator >> (std::istream &strm, Wektor2D &wek){
    char temp;
    strm >> temp;
    if(temp != '('){
		strm.setstate(std::ios::failbit);
	}
    strm >> wek[0];
    strm >> temp;
    if(temp != ','){
		strm.setstate(std::ios::failbit);
	}
    strm >> wek[1];
    strm >> temp;
    if(temp != ')'){
		strm.setstate(std::ios::failbit);
	}
    return strm;
}

/************************************************************
 * Wypisuje wektor na strumien wyjsciowy	           		*
 * Argumenty:												*
 *    strm - strumien										*
 *    wek - wyswietlany wektor      						*
 * Zwraca:													*
 *    Strumien z zapisem wektora w formie (1,2)   			*
 ***********************************************************/
std::ostream& operator << (std::ostream &strm, const Wektor2D &wek){
    strm << "(" << wek[0] << "," << wek[1] << ")";
    return strm; 
}