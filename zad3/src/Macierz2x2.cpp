#include "Macierz2x2.hh"

/************************************************************
 * Realizuje transpozycje macierzy                          *
 * Zwraca:													*
 *    	Transponowana macierz   							*
 ***********************************************************/
Macierz2x2 Macierz2x2::transpozycja()const{
    Macierz2x2 mac_wynik;
    for(int i=0; i<2; i++){
        for(int j=0; j<2; j++){
            mac_wynik.wiersz[i][j]=wiersz[j][i];
        }
    }
    return mac_wynik;
}

/************************************************************
 * Realizuje mnozenie macierzy razy wektor (nie na odwrot)  *
 * Argumenty:                                               *
 *      skl2 - wektor razy ktory mnozymy macierz			*
 * Zwraca:													*
 *    	Wynik mnozenia macierzy razy wektor (tez wektor)    *
 ***********************************************************/
Wektor2D Macierz2x2::operator*(const Wektor2D & skl2) const{
    Wektor2D wek_wynik;
    wek_wynik[0] = wiersz[0]*skl2; //iloczyn skalarny wektora z pierwszego rzedu macierzy i obracanego wektora
    wek_wynik[1] = wiersz[1]*skl2; //analogicznie jak wyzej
    return wek_wynik;
}

/************************************************************
 * Realizuje mnozenie dwoch macierzy                        *
 * Argumenty:                                               *
 *      skl2 - druga macierz             					*
 * Zwraca:													*
 *    	Wynik mnozenia dwuch macierzy (tez macierz)			*
 ***********************************************************/
Macierz2x2 Macierz2x2::operator*(const Macierz2x2 & skl2) const{
    Macierz2x2 mac_wynik;
    Macierz2x2 skl2_transp;
    skl2_transp = skl2.transpozycja();
    for(int i=0; i<2; i++){
        for(int j=0; j<2; j++){
        // iloczyn skalarny wektorow (wierszy z macierzy1 i transponowanej macierzy2)
            mac_wynik.wiersz[i][j]=wiersz[i]*skl2_transp.wiersz[j]; 
        }
    }
    return mac_wynik;
}

/************************************************************
 * Konstruktor klasy Macierz2x2,                            *
 * ustawia macierz domyslnie jako jednostkowa               *
 ***********************************************************/
Macierz2x2::Macierz2x2(){
    for(int i=0; i<2; i++){ 
        for (int j=0; j<2; j++){
            if(i==j){ 
                wiersz[i][j]=1;
            }else{
                wiersz[i][j]=0;
            }
        }
    }
}

/************************************************************
 * Konstruktor klasy Macierz2x2,                            *
 * ustawia wartosc macierzy obrotu zgodnie z podanym katem  *
 ***********************************************************/
Macierz2x2::Macierz2x2(double kat_stopnie){
    double kat_radiany = (kat_stopnie*M_PI)/180;
    wiersz[0][0] = cos(kat_radiany);
    wiersz[0][1] = -sin(kat_radiany);
    wiersz[1][0] = -wiersz[0][1];
    wiersz[1][1] = wiersz[0][0];
}

/************************************************************
 * Wypisuje macierz na strumien wyjsciowy	           		*
 * Argumenty:												*
 *    strm - strumien										*
 *    mac - wyswietlana macierz      						*
 * Zwraca:													*
 *    Strumien z zapisem macierzy w formie:        			*
 *    2     3                                               *
 *    4     5                                               *
 ***********************************************************/
std::ostream& operator << (std::ostream &strm, const Macierz2x2 &mac){
    strm << mac[0][0] << "      " << mac[0][1] << std::endl;
    strm << mac[1][0] << "      " << mac[1][1] << std::endl;
    return strm;
}


