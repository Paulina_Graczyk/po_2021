#include "Drony.hh"

/*!
* \file
* \brief Definicja metod klas modelujących różne drony
*
* Plik zawiera definicje metod 3 klas modelujących 
* 3 różne rodzaje dronów:
* Dron - 4 wirniki
* Dron2 - 3 wirniki
* Dron3 - 2 wirniki
*/

bool Dron::czy_nad(std::shared_ptr<Interfejs_Dron>D){
    Wektor<3> srodek_podst(srodek-Wektor<3>({0,0,srodek[2]}));
    double odleglosc_srodki((srodek_podst-std::dynamic_pointer_cast<Interfejs_elem_kraj>(D)->get_srodek()).dlugosc());

    if((odleglosc_srodki-(get_promien()+D->get_promien())) < 0.0){
        return true;
    } else {
        return false;
    }
}


void Dron::rysuj(drawNS::Draw3DAPI* rysownik){
    korpus.rysuj(rysownik);
    wierniki[0].rysuj(rysownik);
    wierniki[1].rysuj(rysownik);
    wierniki[2].rysuj(rysownik);
    wierniki[3].rysuj(rysownik);
}

void Dron::usun_rys(drawNS::Draw3DAPI* rysownik) const{
    korpus.usun_rys(rysownik);
    wierniki[0].usun_rys(rysownik);
    wierniki[1].usun_rys(rysownik);
    wierniki[2].usun_rys(rysownik);
    wierniki[3].usun_rys(rysownik);

}

void Dron::przekrec_wirniki(drawNS::Draw3DAPI* rysownik){
    Macierz_Rot<3> mac_obr(15,Wektor<3>({0,0,1}));
    usun_rys(rysownik);
    wierniki[0].Obrot(mac_obr);
    wierniki[1].Obrot(mac_obr);
    wierniki[2].Obrot(mac_obr);
    wierniki[3].Obrot(mac_obr);
    rysuj(rysownik);
}

void Dron::lec_przod(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + orientacja*Wektor<3>({0,ile,0});
    rysuj(rysownik);
}

void Dron::lec_gora(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + Wektor<3>({0,0,ile});
    rysuj(rysownik);
}

void Dron::obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    Macierz_Rot<3> Mac_obr(kat_stopnie, Wektor<3>({0,0,1}));
    orientacja = Mac_obr*orientacja;
    rysuj(rysownik);
}
/*********************************************************************/
bool Dron2::czy_nad(std::shared_ptr<Interfejs_Dron>D){
    Wektor<3> srodek_podst(srodek-Wektor<3>({0,0,srodek[2]}));
    double odleglosc_srodki((srodek_podst-std::dynamic_pointer_cast<Interfejs_elem_kraj>(D)->get_srodek()).dlugosc());

    if((odleglosc_srodki-(get_promien()+D->get_promien())) < 0.0){
        return true;
    } else {
        return false;
    }
}

void Dron2::rysuj(drawNS::Draw3DAPI* rysownik){
    korpus.rysuj(rysownik);
    wierniki[0].rysuj(rysownik);
    wierniki[1].rysuj(rysownik);
    wierniki[2].rysuj(rysownik);
}

void Dron2::usun_rys(drawNS::Draw3DAPI* rysownik) const{
    korpus.usun_rys(rysownik);
    wierniki[0].usun_rys(rysownik);
    wierniki[1].usun_rys(rysownik);
    wierniki[2].usun_rys(rysownik);

}

void Dron2::przekrec_wirniki(drawNS::Draw3DAPI* rysownik){
    Macierz_Rot<3> mac_obr(15,Wektor<3>({0,0,1}));
    usun_rys(rysownik);
    wierniki[0].Obrot(mac_obr);
    wierniki[1].Obrot(mac_obr);
    wierniki[2].Obrot(mac_obr);

    rysuj(rysownik);
}

void Dron2::lec_przod(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + orientacja*Wektor<3>({0,ile,0});
    rysuj(rysownik);
}

void Dron2::lec_gora(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + Wektor<3>({0,0,ile});
    rysuj(rysownik);
}

void Dron2::obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    Macierz_Rot<3> Mac_obr(kat_stopnie, Wektor<3>({0,0,1}));
    orientacja = Mac_obr*orientacja;
    rysuj(rysownik);
}
/*******************************************************************************/
bool Dron3::czy_nad(std::shared_ptr<Interfejs_Dron>D){
    Wektor<3> srodek_podst(srodek-Wektor<3>({0,0,srodek[2]}));
    double odleglosc_srodki((srodek_podst-std::dynamic_pointer_cast<Interfejs_elem_kraj>(D)->get_srodek()).dlugosc());

    if((odleglosc_srodki-(get_promien()+D->get_promien())) < 0.0){
        return true;
    } else {
        return false;
    }
}

void Dron3::rysuj(drawNS::Draw3DAPI* rysownik){
    korpus.rysuj(rysownik);
    wierniki[0].rysuj(rysownik);
    wierniki[1].rysuj(rysownik);
}

void Dron3::usun_rys(drawNS::Draw3DAPI* rysownik) const{
    korpus.usun_rys(rysownik);
    wierniki[0].usun_rys(rysownik);
    wierniki[1].usun_rys(rysownik);

}

void Dron3::przekrec_wirniki(drawNS::Draw3DAPI* rysownik){
    Macierz_Rot<3> mac_obr(15,Wektor<3>({0,0,1}));
    usun_rys(rysownik);
    wierniki[0].Obrot(mac_obr);
    wierniki[1].Obrot(mac_obr);
    rysuj(rysownik);
}

void Dron3::lec_przod(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + orientacja*Wektor<3>({0,ile,0});
    rysuj(rysownik);
}

void Dron3::lec_gora(double ile,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    srodek = srodek + Wektor<3>({0,0,ile});
    rysuj(rysownik);
}

void Dron3::obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik){
    usun_rys(rysownik);
    Macierz_Rot<3> Mac_obr(kat_stopnie, Wektor<3>({0,0,1}));
    orientacja = Mac_obr*orientacja;
    rysuj(rysownik);
}