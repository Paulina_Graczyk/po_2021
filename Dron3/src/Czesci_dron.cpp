#include "Czesci_dron.hh"

/*!
* \file
* \brief Definicja metod klas Uklad_W, Prostopadloscian i Graniastoslup6
*
* Plik zawiera definicje metod klas Uklad_w, 
* Prostopadloscian, Graniastoslup6 potrzebnych do stworzenia drona
* oraz funkcje konwersji na Point3D
*/

Uklad_W Uklad_W::Przelicz_uklad_do_globalnego()const{
    Uklad_W w_globalnym(Wektor<3>({0,0,0}), Macierz_Rot<3>(), nullptr);
    Uklad_W tmp(*this);
    while(tmp.rodzic != nullptr){
        w_globalnym.srodek = (tmp.rodzic->orientacja * tmp.srodek) + tmp.rodzic->srodek;
        w_globalnym.orientacja = tmp.rodzic->orientacja * tmp.orientacja;
        tmp.srodek = w_globalnym.srodek;
        tmp.orientacja = w_globalnym.orientacja;
        tmp.rodzic = tmp.rodzic->rodzic;
    }
    if(tmp.rodzic == nullptr){
        return tmp;
    }
    return w_globalnym;
}


Wektor<3> Uklad_W::Przelicz_wierzcholek_do_globalnego(Wektor<3> wierzcholek_lok) const{
    Uklad_W globalny = Przelicz_uklad_do_globalnego();
    Wektor<3> wynik;
    wynik = (globalny.orientacja * wierzcholek_lok) + globalny.srodek;
    return wynik;
}
/*******************************************************************************/

void Prostopadloscian::rysuj(drawNS::Draw3DAPI* rysownik){
    Wektor<3> wierzcholek1, wierzcholek2,wierzcholek3,wierzcholek4,wierzcholek5,wierzcholek6,wierzcholek7,wierzcholek8;
  
    wierzcholek1 = Przelicz_wierzcholek_do_globalnego(Wektor<3>({0.5*szerokosc,0.5*glebokosc,0.5*wysokosc}));
    wierzcholek2 = Przelicz_wierzcholek_do_globalnego(Wektor<3>({0.5*szerokosc,-0.5*glebokosc,0.5*wysokosc}));
    wierzcholek3 = Przelicz_wierzcholek_do_globalnego(Wektor<3>({-0.5*szerokosc,-0.5*glebokosc,0.5*wysokosc}));
    wierzcholek4 = Przelicz_wierzcholek_do_globalnego(Wektor<3>({-0.5*szerokosc,0.5*glebokosc,0.5*wysokosc}));
    wierzcholek5 = Przelicz_wierzcholek_do_globalnego(Wektor<3>({0.5*szerokosc,0.5*glebokosc,-0.5*wysokosc}));
    wierzcholek6 = Przelicz_wierzcholek_do_globalnego(Wektor<3>({0.5*szerokosc,-0.5*glebokosc,-0.5*wysokosc}));
    wierzcholek7 = Przelicz_wierzcholek_do_globalnego(Wektor<3>({-0.5*szerokosc,-0.5*glebokosc,-0.5*wysokosc}));
    wierzcholek8 = Przelicz_wierzcholek_do_globalnego(Wektor<3>({-0.5*szerokosc,0.5*glebokosc,-0.5*wysokosc}));

    
    id = rysownik->draw_polyhedron(std::vector<std::vector<drawNS::Point3D> > {{
      konwertuj(wierzcholek1),konwertuj(wierzcholek2),
      konwertuj(wierzcholek3),konwertuj(wierzcholek4)
      },{
      konwertuj(wierzcholek5),konwertuj(wierzcholek6),
      konwertuj(wierzcholek7),konwertuj(wierzcholek8)
      }},"purple");

}

void Prostopadloscian::usun_rys(drawNS::Draw3DAPI* rysownik) const{
    rysownik->erase_shape(id);
}
/*******************************************************************************/

void Graniastoslup6::rysuj(drawNS::Draw3DAPI* rysownik){
    std::array<Wektor<3>,12> wierzcholki;
    double r = 0.5*bok_podst*sqrt(3);

    wierzcholki[0] = Przelicz_wierzcholek_do_globalnego(Wektor<3>({bok_podst,0,-0.5*wysokosc})); //bok_podst = R
    wierzcholki[1] = Przelicz_wierzcholek_do_globalnego(Wektor<3>({0.5*bok_podst,r,-0.5*wysokosc}));
    wierzcholki[2] = Przelicz_wierzcholek_do_globalnego(Wektor<3>({-0.5*bok_podst,r,-0.5*wysokosc}));
    wierzcholki[3] = Przelicz_wierzcholek_do_globalnego(Wektor<3>({-bok_podst,0,-0.5*wysokosc}));
    wierzcholki[4] = Przelicz_wierzcholek_do_globalnego(Wektor<3>({-0.5*bok_podst,-r,-0.5*wysokosc}));
    wierzcholki[5] = Przelicz_wierzcholek_do_globalnego(Wektor<3>({0.5*bok_podst,-r,-0.5*wysokosc}));
    
    for(int i=0;i<6;i++){
        wierzcholki[6+i] = wierzcholki[i]+Wektor<3>({0,0,wysokosc});
    }
    

    id = rysownik->draw_polyhedron(std::vector<std::vector<drawNS::Point3D> > {{
      konwertuj(wierzcholki[0]),konwertuj(wierzcholki[1]),
      konwertuj(wierzcholki[2]),konwertuj(wierzcholki[3]),
      konwertuj(wierzcholki[4]),konwertuj(wierzcholki[5]),
      },{
      konwertuj(wierzcholki[6]),konwertuj(wierzcholki[7]),
      konwertuj(wierzcholki[8]),konwertuj(wierzcholki[9]),
      konwertuj(wierzcholki[10]),konwertuj(wierzcholki[11]),
      }},"purple");
}


void Graniastoslup6::usun_rys(drawNS::Draw3DAPI* rysownik) const{
    rysownik->erase_shape(id);
}
/*******************************************************************************/

  drawNS::Point3D konwertuj(Wektor<3> W){
      return drawNS::Point3D(W[0],W[1],W[2]);
}

