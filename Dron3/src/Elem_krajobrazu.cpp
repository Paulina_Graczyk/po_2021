#include "Elem_krajobrazu.hh"
/*!
* \file
* \brief Definicja metod klas Plaskowyz, Wzgorze i Plaskowyz_prost
*
* Plik zawiera definicje metod klas Plaskowyz, 
* Wzgorze, Plaskowyz_prost tworzących elementy krajobrazu
* oraz funkcje losowania random
*/


uint random(uint min, uint max){
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_int_distribution<> distribution(min,max);
    return distribution(generator);
}

bool Plaskowyz::czy_nad(std::shared_ptr<Interfejs_Dron>D){
    double R_max(0);
    double R_tmp(0);
    Wektor<3> srodek_podst(srodek);
    Wektor<3> srodek_dron(std::dynamic_pointer_cast<Interfejs_elem_kraj>(D)->get_srodek());
    Wektor<3> odleglosc_srodki(srodek_podst-srodek_dron+Wektor<3>({0,0,srodek_dron[2]}));
    for(int i=0; i< wierzcholki_dol.size();i++){
        R_tmp = (wierzcholki_dol[i]-srodek_podst).dlugosc();
        if(R_tmp>R_max){
            R_max = R_tmp;
        }
    }
    if((odleglosc_srodki.dlugosc()-(R_max+D->get_promien())) < 0.0){
        return true;
    } else {
        return false;
    }
}

bool Plaskowyz::czy_ladowac(std::shared_ptr<Interfejs_Dron>D, double & wys) {
    Wektor<3> srodek_podst(srodek);
    double R_ok;
    Wektor<3> srodek_dron(std::dynamic_pointer_cast<Interfejs_elem_kraj>(D)->get_srodek());
    double r_min(100);
    double R_tmp(0);
    for(int i=0; i< wierzcholki_dol.size();i++){
        R_tmp = (wierzcholki_dol[i]-srodek_podst).dlugosc();
        if(R_tmp<r_min){
            r_min = R_tmp;
        }
    }
    R_ok = r_min-D->get_promien();
    if((srodek_podst-srodek_dron-Wektor<3>({0,0,srodek_dron[2]})).dlugosc()-R_ok<=0.0){
        wys = wysokosc+D->get_wysokosc()/2;
        return true;
    } else {
        return false;
    }
}

void Plaskowyz::rysuj(drawNS::Draw3DAPI *rysownik){
    std::vector<std::vector<drawNS::Point3D>> wierzch_wszystkie;
    std::vector<drawNS::Point3D> wierzch_dol_konw;
    std::vector<drawNS::Point3D> wierzch_gora_konw;

    for(uint i=0; i<wierzcholki_dol.size(); i++){
        wierzch_dol_konw.push_back(konwertuj(wierzcholki_dol[i]));
    }

    for(uint i=0; i<wierzcholki_dol.size(); i++){
        wierzch_gora_konw.push_back(konwertuj(wierzcholki_dol[i]+Wektor<3>({0,0,wysokosc})));
    }

    wierzch_wszystkie.push_back(wierzch_dol_konw);
    wierzch_wszystkie.push_back(wierzch_gora_konw);

    id_rys = rysownik->draw_polyhedron(wierzch_wszystkie, "grey");

}

void Plaskowyz::usun_rys(drawNS::Draw3DAPI* rysownik)const{
    rysownik->erase_shape(id_rys);
}

void Wzgorze::rysuj(drawNS::Draw3DAPI *rysownik){
    std::vector<std::vector<drawNS::Point3D>> wierzch_wszystkie;
    std::vector<drawNS::Point3D> wierzch_dol_konw;
    std::vector<drawNS::Point3D> wierzch_gora_konw;

    for(uint i=0; i<wierzcholki_dol.size(); i++){
        wierzch_dol_konw.push_back(konwertuj(wierzcholki_dol[i]));
    }

    for(uint i=0; i<wierzcholki_dol.size(); i++){
        wierzch_gora_konw.push_back(konwertuj(srodek+Wektor<3>({0,0,wysokosc})));
    }
   
    wierzch_wszystkie.push_back(wierzch_dol_konw);
    wierzch_wszystkie.push_back(wierzch_gora_konw);

    id_rys = rysownik->draw_polyhedron(wierzch_wszystkie, "grey");

}

bool Wzgorze::czy_nad(std::shared_ptr<Interfejs_Dron>D){
    double R_max(0);
    double R_tmp(0);
    Wektor<3> srodek_podst(srodek);
    Wektor<3> srodek_dron(std::dynamic_pointer_cast<Interfejs_elem_kraj>(D)->get_srodek());
    Wektor<3> odleglosc_srodki(srodek_podst-srodek_dron+Wektor<3>({0,0,srodek_dron[2]}));
    for(int i=0; i< wierzcholki_dol.size();i++){
        R_tmp = (wierzcholki_dol[i]-srodek_podst).dlugosc();
        if(R_tmp>R_max){
            R_max = R_tmp;
        }
    }
    if((odleglosc_srodki.dlugosc()-(R_max+D->get_promien())) < 0.0){
        return true;
    } else {
        return false;
    }
}

void Wzgorze::usun_rys(drawNS::Draw3DAPI* rysownik)const{
    rysownik->erase_shape(id_rys);
}

bool Plaskowyz_prost::czy_nad(std::shared_ptr<Interfejs_Dron>D){
Wektor<3> srodek_dron (std::dynamic_pointer_cast<Interfejs_elem_kraj>(D)->get_srodek());
    if(abs(srodek_dron[0]-srodek[0])>(szerokosc/2+D->get_promien())){
        return false;
    }
    if(abs(srodek_dron[1]-srodek[1])>(glebokosc/2+D->get_promien())){
        return false;
    }
    return true;
}

bool Plaskowyz_prost::czy_ladowac(std::shared_ptr<Interfejs_Dron>D, double & wys) {
    Wektor<3> srodek_dron (std::dynamic_pointer_cast<Interfejs_elem_kraj>(D)->get_srodek());
    if(abs(srodek_dron[0]-srodek[0])>(szerokosc/2)){
        return false;
    }
    if(abs(srodek_dron[1]-srodek[1])>(glebokosc/2)){
        return false;
    }
    wys = wysokosc+D->get_wysokosc()/2;
    return true;
}

void Plaskowyz_prost::rysuj(drawNS::Draw3DAPI* rysownik){
    std::vector<std::vector<drawNS::Point3D>> wierzch_wszystkie;
    std::vector<drawNS::Point3D> wierzch_dol_konw;
    std::vector<drawNS::Point3D> wierzch_gora_konw;
    std::vector<Wektor<3>> wierzch_dol;

    
    wierzch_dol.push_back(Wektor<3>({0.5*szerokosc,0.5*glebokosc,0})+srodek);
    wierzch_dol.push_back(Wektor<3>({0.5*szerokosc,-0.5*glebokosc,0})+srodek);
    wierzch_dol.push_back(Wektor<3>({-0.5*szerokosc,-0.5*glebokosc,0})+srodek);
    wierzch_dol.push_back(Wektor<3>({-0.5*szerokosc,0.5*glebokosc,0})+srodek);

    for(int i=0; i<4; i++){
        wierzch_dol_konw.push_back(konwertuj(wierzch_dol[i]));
    }
    for(int i=0; i<4; i++){
        wierzch_gora_konw.push_back(konwertuj(wierzch_dol[i]+Wektor<3>({0,0,wysokosc})));
    }

    wierzch_wszystkie.push_back(wierzch_dol_konw);
    wierzch_wszystkie.push_back(wierzch_gora_konw);

    id = rysownik->draw_polyhedron(wierzch_wszystkie, "grey");

}

void Powierzchnia::rysuj(drawNS::Draw3DAPI* rysownik){
    std::vector<std::vector<drawNS::Point3D>> tmp1;
    std::vector<drawNS::Point3D> tmp2;
    for(int i=-20; i<=20; i+=5){
        for (int j=-20; j<=20; j+=5){
            tmp2.push_back(drawNS::Point3D(i,j, wysokosc));
        }
        tmp1.push_back(tmp2);
        tmp2.clear();
    }
    id_rys = rysownik->draw_surface(tmp1,"grey");
}
