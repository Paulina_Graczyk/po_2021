#ifndef INTERFEJSY_HH
#define INTERFEJSY_HH

/*!
* \file
* \brief Definicje klas wirtualnych będących interfejsami
*
* Plik zawiera definicję klas wirtualnych będących interfejsami:
* rysowania, drona i elementów krajobrazu
*/

#include "Dr3D_gnuplot_api.hh"

/*!
* \brief Modeluje pojęcie interfejsu rysowania
* 
* Odpowiada za rysowanie i usuwanie rysunków
*/
class Interfejs_rysowanie{
public:
/*!
* \brief rysuje rysunek odpowiedniego kształtu w gnuplocie
* \param[in]    rysownik    wskaźnik potrzebny do rysowania za pomocą Dr3D_gnuplot_api
*/
    virtual void rysuj(drawNS::Draw3DAPI* rysownik) = 0;
/*!
* \brief usuwa rysunek po jego id
* \param[in]    rysownik    wskaźnik potrzebny do rysowania za pomocą Dr3D_gnuplot_api
*/
    virtual void usun_rys(drawNS::Draw3DAPI* rysownik)const=0;
};
/**************************************************************************************/
/*!
* \brief Modeluje pojęcie interfejsu drona
* 
* Odpowiada za komunikację sceny z dronem
*/
class Interfejs_Dron{
public:
/*!
* \brief przekręca wszystkie wirniki o 15 stopni i rysuje dron
* \param[in]    rysownik    wskaźnik potrzebny do rysowania za pomocą Dr3D_gnuplot_api
*/
    virtual void przekrec_wirniki(drawNS::Draw3DAPI* rysownik)=0;
/*!
* \brief przesuwa środek drona o zadana wartość w przód i rysuje dron
* \param[in]    ile wartość o jaką ma się przenieść w przód
* \param[in]    rysownik    wskaźnik potrzebny do rysowania za pomocą Dr3D_gnuplot_api
*/
    virtual void lec_przod(double ile,drawNS::Draw3DAPI* rysownik)=0;
/*!
* \brief przesuwa środek drona o zadana wartość w górę i rysuje dron
* \param[in]    ile wartość o jaką ma się przenieść w górę
* \param[in]    rysownik    wskaźnik potrzebny do rysowania za pomocą Dr3D_gnuplot_api
*/
    virtual void lec_gora(double ile,drawNS::Draw3DAPI* rysownik)=0;
/*!
* \brief  obraca dron o zadany kąt
* \param[in]    kat_stopnie wartość kąta w stopniach
* \param[in]    rysownik    wskaźnik potrzebny do rysowania za pomocą Dr3D_gnuplot_api
*/
    virtual void obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik)=0;
/*!
* \brief udostępnia odległość od środka do wierchołka korpusu
* Potrzebne do sprawdzania możliwości lądowania
* \return odległość od środka do wierchołka korpusu
*/
    virtual double get_promien()=0;
/*!
* \brief udostępnia wysokość korpusu drona
* Potrzebne do animacji lądowania
* \return wysokość korpusu drona
*/
    virtual double get_wysokosc()=0;

};

/**************************************************************************************/
/*!
* \brief Modeluje pojęcie interfejsu elementów krajobrazu
* Odpowiada za sprawdzenie możliwości lądowania drona
*/

class Interfejs_elem_kraj{
public:
/*!
* \brief sprawdza czy nad elementem znajduje dron
* \param[in]    D wskaźnik na dron który jest sprawdzany
* \retval   true jeśli się znajduje 
* \retval   false jeśli się nie znajduje
*/
    virtual bool czy_nad(std::shared_ptr<Interfejs_Dron>D)=0;
/*!
* \brief zwraca informacje czy na tym elemencie można lądować
* \param[in]    D wskaźnik na dron który jest sprawdzany
* \param[out]    wys wysokość na jakiej może lądować
* \retval   true jeśli może
* \retval   false jeśli nie może
*/
    virtual bool czy_ladowac(std::shared_ptr<Interfejs_Dron> D, double & wys)=0;
/*!
* \brief udostępnia srodek elementu
* Potrzebne do animacji lotu i wypisywania elementów krajobrazu
* \return srodek elementu
*/
    virtual Wektor<3> get_srodek()=0;
};

#endif