#ifndef DRONY_HH
#define DRONY_HH

/*!
* \file
* \brief Definicje klas modelujących różne drony
*
* Plik zawiera definicję 3 klas modelujących 
* 3 różne rodzaje dronów:
* Dron - 4 wirniki
* Dron2 - 3 wirniki
* Dron3 - 2 wirniki
*/

#include "Wektor.hh"
#include "Macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include "Interfejsy.hh"
#include "Czesci_dron.hh"
#include <iostream>
#include <array>

/*!
* \brief Modeluje pojęcie drona z 4 wirnikami
*/
class Dron: public Interfejs_rysowanie, protected Uklad_W, public Interfejs_Dron, public Interfejs_elem_kraj {
/*!
* \brief korpus dronu będący prostopadłościanem
*/
    Prostopadloscian korpus;
/*!
* \brief tablica 4 graniastosłupów będących wirnikami
*/
    std::array<Graniastoslup6, 4> wierniki;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik) const override;
    void przekrec_wirniki(drawNS::Draw3DAPI* rysownik) override;
    void lec_przod(double ile,drawNS::Draw3DAPI* rysownik) override;
    void lec_gora(double ile,drawNS::Draw3DAPI* rysownik) override;
    void obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik)override;
    bool czy_nad(std::shared_ptr<Interfejs_Dron> D)override;
    bool czy_ladowac(std::shared_ptr<Interfejs_Dron> D, double & wys)override{return false;};
    Wektor<3> get_srodek() override {return srodek;};
    double get_promien() override {return korpus.get_promien();};
    double get_wysokosc() override {return korpus.get_wysokosc();};
/*!
* \brief Kontruktor - definiuje drona składającego się prostopadłościennego korpusu i 4 wirników
* \param[in]    sro   srodek drona
* \param[in]    Orient   orientacja drona
*/
    Dron(Wektor<3> Sro, Macierz_Rot<3> Orient):
    Uklad_W(Sro, Orient, nullptr),
    korpus(Wektor<3>({0,0,0}), Macierz_Rot<3>(), this, 2, 3, 1), 
    wierniki({Graniastoslup6(Wektor<3>({1,1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor</*!
* \brief Modeluje pojęcie drona z 4 wirnikami
*/3>({1,-1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor<3>({-1,-1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor<3>({-1,1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8)}) {};

};

/*******************************************************************************/
/*!
* \brief Modeluje pojęcie drona z 3 wirnikami
*/
class Dron2: public Interfejs_rysowanie, protected Uklad_W, public Interfejs_Dron, public Interfejs_elem_kraj {
/*!
* \brief korpus dronu będący prostopadłościanem
*/
    Prostopadloscian korpus;
/*!
* \brief tablica 3 graniastosłupów będących wirnikami
*/
    std::array<Graniastoslup6, 3> wierniki;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik) const override;
    void przekrec_wirniki(drawNS::Draw3DAPI* rysownik) override;
    void lec_przod(double ile,drawNS::Draw3DAPI* rysownik) override;
    void lec_gora(double ile,drawNS::Draw3DAPI* rysownik) override;
    void obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik)override;
    bool czy_nad(std::shared_ptr<Interfejs_Dron> D)override;
    bool czy_ladowac(std::shared_ptr<Interfejs_Dron> D, double & wys)override{return false;};
    Wektor<3> get_srodek() override {return srodek;};
    double get_promien() override {return korpus.get_promien();};
    double get_wysokosc() override {return korpus.get_wysokosc();};
/*!
* \brief Kontruktor - definiuje drona składającego się prostopadłościennego korpusu i 3 wirników
* \param[in]    sro   srodek drona
* \param[in]    Orient   orientacja drona
*/
    Dron2(Wektor<3> Sro, Macierz_Rot<3> Orient):
    Uklad_W(Sro, Orient, nullptr),
    korpus(Wektor<3>({0,0,0}), Macierz_Rot<3>(), this, 2, 3, 1), 
    wierniki({Graniastoslup6(Wektor<3>({1,1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor<3>({-1,1.5,0.7}),Macierz_Rot<3>(),this,0.4,0.8),
    Graniastoslup6(Wektor<3>({0,-1.5,0.7}),Macierz_Rot<3>(),this,0.6,1)}) {};

};
/*******************************************************************************/
class Dron3: public Interfejs_rysowanie, protected Uklad_W, public Interfejs_Dron, public Interfejs_elem_kraj {
    Prostopadloscian korpus;
    std::array<Graniastoslup6, 2> wierniki;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik) const override;
    void przekrec_wirniki(drawNS::Draw3DAPI* rysownik) override;
    void lec_przod(double ile,drawNS::Draw3DAPI* rysownik) override;
    void lec_gora(double ile,drawNS::Draw3DAPI* rysownik) override;
    void obroc(double kat_stopnie,drawNS::Draw3DAPI* rysownik)override;
    bool czy_nad(std::shared_ptr<Interfejs_Dron> D)override;
    bool czy_ladowac(std::shared_ptr<Interfejs_Dron> D, double & wys)override{return false;};
    Wektor<3> get_srodek() override {return srodek;};
    double get_promien() override {return korpus.get_promien();};
    double get_wysokosc() override {return korpus.get_wysokosc();};
/*!
* \brief Kontruktor - definiuje drona składającego się prostopadłościennego korpusu i 2 wirników
* \param[in]    sro   srodek drona
* \param[in]    Orient   orientacja drona
*/
    Dron3(Wektor<3> Sro, Macierz_Rot<3> Orient):
    Uklad_W(Sro, Orient, nullptr),
    korpus(Wektor<3>({0,0,0}), Macierz_Rot<3>(), this, 2, 4, 1), 
    wierniki({Graniastoslup6(Wektor<3>({0,1.7,0.7}),Macierz_Rot<3>(),this,0.8,1.2),
    Graniastoslup6(Wektor<3>({0,-1.7,0.7}),Macierz_Rot<3>(),this,0.8,1.2)}) {};
};



#endif