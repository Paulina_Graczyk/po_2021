#ifndef ELEMKRAJ_HH
#define ELEMKRAJ_HH

/*!
* \file
* \brief Definicje klas modelujących różne elementy krajobrazu
*
* Plik zawiera definicję 3 klas modelujących 
* 3 różne rodzaje elementów krajobrazu:
* Plaskowyz - graniastosłup o losowawnej iloci kątów 
* Wzgórze - ostrosłup o losowawnej iloci kątów 
* Pladkowyz_prost - prostopadłościan
* oraz funkcię losowania random
*/

#include "Czesci_dron.hh"
#include "Interfejsy.hh"
#include "Dr3D_gnuplot_api.hh"
#include <iostream>
#include <stdio.h>
#include <random>

/*!
* \brief Losuje liczbę całkowitą z przedziału (min,max)
* \param min minimalna ilosc wierzchołków/wysokość
* \param max maksymalna ilosc wierzchołków/wysokość
* \return wylosowaną liczbę z przedziału
*/
uint random(uint min, uint max);


/*!
* \brief Modeluje pojęcie klasy Plaskowyz
* Modeluje pojęcie pojęcie klasy Plaskowyz jako 
* elemtu krajobrazu w kształcie graniastosłup o losowawnej iloci kątów 
*/
class Plaskowyz : public Uklad_W, public Interfejs_rysowanie, public Interfejs_elem_kraj {
/*!
* \brief kontener ze współrzędnymi dolnych wierzchołków 
*/
    std::vector<Wektor<3>> wierzcholki_dol;
/*!
* \brief wysokość plaskowyżu
*/
    double wysokosc;
/*!
* \brief id generowane podczas rysowania
*/
    int id_rys;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik)const override;
    bool czy_nad(std::shared_ptr<Interfejs_Dron>D) override;
    bool czy_ladowac(std::shared_ptr<Interfejs_Dron>D, double & wys) override;
    Wektor<3> get_srodek() override {return srodek;};
/*!
* \brief konstruktor
* \param Srodek środek dolnej podstawy plaskowyżu
* \param min minimalna ilosc wierzchołków/wysokość
* \param max maksymalna ilosc wierzchołków/wysokość
*/
    Plaskowyz(Wektor<3> Srodek, uint min, uint max):Uklad_W(Srodek,Macierz_Rot<3>(),nullptr),
    wysokosc(random(min,max)) , 
    id_rys(-1) {
    uint ilosc_wierzch(random(min,max));
    Macierz_Rot<3> rot(360/ilosc_wierzch,Wektor<3>({0,0,1}));
    Macierz_Rot<3> mac_pom;
    for(uint i=0; i<ilosc_wierzch; i++){
        Wektor<3> R({random(min,max),0,0});
        wierzcholki_dol.push_back(Srodek + mac_pom*R); 
        mac_pom=rot*mac_pom;
    }};
};
/***********************************************************************************/
/*!
* \brief Modeluje pojęcie klasy Wzgorze
* Modeluje pojęcie pojęcie klasy Wzgorze jako 
* elemtu krajobrazu w kształcie ostrosłupa o losowawnej iloci kątów 
*/
class Wzgorze : public Uklad_W, public Interfejs_rysowanie, public Interfejs_elem_kraj {
/*!
* \brief kontener ze współrzędnymi dolnych wierzchołków 
*/
    std::vector<Wektor<3>> wierzcholki_dol;
/*!
* \brief wysokość wzgórza
*/
    double wysokosc;
/*!
* \brief id generowane podczas rysowania
*/
    int id_rys;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik)const override;
    bool czy_nad(std::shared_ptr<Interfejs_Dron>D) override;
    bool czy_ladowac(std::shared_ptr<Interfejs_Dron>D, double & wys) override{return false;};
    Wektor<3> get_srodek() override {return srodek;};
/*!
* \brief konstruktor
* \param Srodek środek dolnej podstawy wzgórza
* \param min minimalna ilosc wierzchołków/wysokość
* \param max maksymalna ilosc wierzchołków/wysokość
*/
    Wzgorze(Wektor<3> Srodek, uint min, uint max):Uklad_W(Srodek,Macierz_Rot<3>(),nullptr),
    wysokosc(random(min,max)),
    id_rys(-1) {
    uint ilosc_wierzch(random(min,max));
    Macierz_Rot<3> rot(360/ilosc_wierzch,Wektor<3>({0,0,1}));
    Macierz_Rot<3> mac_pom;
    for(uint i=0; i<ilosc_wierzch; i++){
        Wektor<3> R({random(min,max),0,0});
        wierzcholki_dol.push_back(Srodek + mac_pom*R); 
        mac_pom=rot*mac_pom;
    }};
};
/***********************************************************************************/
/*!
* \brief Modeluje pojęcie klasy Plaskowyz_prost
* Modeluje pojęcie pojęcie klasy Plaskowyz_prost jako 
* elemtu krajobrazu w kształcie prostopadłościanu
*/
class Plaskowyz_prost : public Prostopadloscian, public Interfejs_elem_kraj {
public:
    void rysuj(drawNS::Draw3DAPI* rysownik)override;
    bool czy_nad(std::shared_ptr<Interfejs_Dron>D) override;
    bool czy_ladowac(std::shared_ptr<Interfejs_Dron>D, double & wys) override;
    Wektor<3> get_srodek() override {return srodek;};
/*!
* \brief konstruktor
* \param Srodek środek dolnej podstawy plaskowyzu prostopadłościennego
* \param min minimalna ilosc wierzchołków/wysokość
* \param max maksymalna ilosc wierzchołków/wysokość
*/
    Plaskowyz_prost(Wektor<3> Srodek, uint min, uint max): 
    Prostopadloscian(Srodek,Macierz_Rot<3>(),nullptr,random(min,max),random(min,max),random(min,max)){};
};
/***********************************************************************************/
/*!
* \brief Modeluje pojęcie klasy Powierzchnia
* Modeluje pojęcie pojęcie klasy Powierzchnia jako 
* plaskiej powierzchni symmbolizującej powierzchnię gruntu
*/
class Powierzchnia : public Interfejs_rysowanie{
/*!
* \brief wysokość wzgórza
*/
    double wysokosc;
/*!
* \brief id generowane podczas rysowania
*/
    int id_rys;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik)const override{rysownik->erase_shape(id_rys);};
/*!
* \brief konstruktor
* \param Wys wysokość na której ma znajdować się powierzchnia
*/
    Powierzchnia(double Wys): wysokosc(Wys) {};
};
#endif