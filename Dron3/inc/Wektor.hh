#ifndef WEKTOR_HH
#define WEKTOR_HH

/*!
* \file
* \brief Definicja klasy Wektor
*
* Plik zawiera definicję klasy Wektor
* oraz przeciążeń operatorów >> i <<
*/

#include <iostream>
#include <array>
#include <cmath>

/*!
* \brief Modeluje pojęcie Wektora
*
* KLasa modeluje pojęcie wektora o zadanym wymiarze
* oraz zlicza utworzone i istniejące wektory
*/
template <int ROZMIAR>
class Wektor {
/*!
* \brief Tablica współrzędnych wektora
*/
  std::array<double,ROZMIAR> wsp;
/*!
* \brief ilosc istniejących wektorów
*/
  static uint ile_jest;
/*!
* \brief Ilość stworzonych wektorów
*/
  static uint ile_stworzono;
public:
/*!
* \brief Realizuje dodanie wektorow
* \param[in]    skl2    drugi skladnik sumy
* \return sume dwoch wektorow
*/
  Wektor<ROZMIAR> operator+(const Wektor<ROZMIAR> & skl2) const;
/*!
* \brief Realizuje odejmowanie wektorow
* \param[in]    skl2    drugi skladnik różnicy
* \return różnicę dwoch wektorow
*/
  Wektor<ROZMIAR> operator-(const Wektor<ROZMIAR> & skl2) const;
/*!
* \brief Realizuje mnozenie wektora razy liczbe
* \param[in]    skl2    liczba razy ktora mnozymy wektor
* \return Iloczyn wektora i liczby
*/
  Wektor<ROZMIAR> operator*(double skl2) const;
/*!
* \brief Realizuje iloczyn skalarny wektorow 
* \param[in]    skl2    drugi skladnik iloczynu 
* \return Iloczyn skalarny dwoch wektorow 
*/
  double operator*(const Wektor<ROZMIAR> & skl2) const;
/*!
*  \brief Oblicza dlugosc wektora 
* \return Dlugosc wektora  
*/
  double dlugosc() const;
/*!
* \brief Konstruktor, dodaje do liczby stworzonych i istniejacych wektorow
*/
  Wektor() { ile_jest++; ile_stworzono++;};
/*!
* \brief Konstruktor, dodaje do liczby stworzonych i istniejacych wektorow
* \param arg tablica ze współrzędnymi
*/
  Wektor(std::array<double,ROZMIAR> arg): wsp(arg) {ile_jest++; ile_stworzono++;};
/*!
* \brief Konstruktor kopiujący, dodaje do liczby stworzonych i istniejacych wektorow
* \param arg wskaźnik na wektor
*/
  Wektor(const Wektor &W): wsp(W.wsp) {ile_jest++;ile_stworzono++;};
/*!
* \brief Destruktor, zmniejsza liczbe istniejących wektorów
*/
  ~Wektor(){ile_jest--;};
/*!
* \brief  Udostępnia liczbę isniejących wektorów
* \return liczbę istniejących wektorów
*/
  static uint get_ile_jest() {return ile_jest; };
/*!
* \brief  Udostępnia liczbę stworzonych wektorów
* \return liczbę stworzonych wektorów
*/
  static uint get_ile_stworzono() {return ile_stworzono;};
/*!
* \brief  geter, udostępnia poszczególne współrzędne wektora
* \param i numer współrzędnej do zwrócenia
* \return zadaną współrzędną
*/
  const double & operator[] (int i) const{
    if(i<0||i>ROZMIAR-1){std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wsp[i];}
/*!
* \brief  seter, udostępnia poszczególne współrzędne wektora
* \param i numer współrzędnej do zwrócenia
* \return zadaną współrzędną
*/
  double & operator[](int i){
    if(i<0||i>ROZMIAR-1){std::cerr << "poza pamiecia" << std::endl; exit(1);}
    return wsp[i];}
};
/*!
* Wczytuje wektor ze strumienia wejsciowego   
* \param[in]    strm    strumien
* \param[in]    wek    wczytywany wektor 
* \return Strumien z wczytanym wektorem  
*/
template <int ROZMIAR>
std::istream& operator >> (std::istream &strm, Wektor<ROZMIAR> &wek);
/*!
* Wypisuje wektor na strumien wyjsciowy  
* \param[in]    strm    strumien
* \param[in]    wek    wyswietlany wektor
* \return Strumien z zapisem wektora w formie (1,2)  
*/
template <int ROZMIAR>
std::ostream& operator << (std::ostream &strm, const Wektor<ROZMIAR> &wek);

#endif
