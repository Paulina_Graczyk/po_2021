#ifndef UKLADW_HH
#define UKLADW_HH

/*!
* \file
* \brief Definicje klas potrzebnych do stworznia drona
*
* Plik zawiera definicję klas potrzebnych do stwoerznia drona
* są to: Uklad_W, Prostopadloscian, Graniastoslup6
* oraz funkcja do konwersji na Point3D
*/

#include "Wektor.hh"
#include "Macierz.hh"
#include "Dr3D_gnuplot_api.hh"
#include "Interfejsy.hh"
#include <iostream>
#include <array>

/*!
* \brief Modeluje układ współrzednych
*
* KLasa modeluje pojęcie układu współrzędnych
*/
class Uklad_W {
protected:
/*!
* \brief srodek ukladu wspolrzędnych
*/
    Wektor<3> srodek;
/*!
* \brief orientacja ukladu wspolrzędnych
*/
    Macierz_Rot<3> orientacja;
/*!
* \brief Układ nadrzędny dla danego układu
*/
    Uklad_W * rodzic;
public:
/*!
* \brief Realizuje przesunięcie środka układu o wektor
* \param[in]    Wek_przes    wektor o który chcemy przesunąć
*/
    void Przesuniecie(Wektor<3> Wek_przes) {srodek = srodek + Wek_przes;};
/*!
* \brief Realizuje obrót układu za pomocą danej macierzy rotacji
* \param[in]    Mac_obr    macierz obrotu
*/
    void Obrot(Macierz_Rot<3> Mac_obr) {orientacja = Mac_obr * orientacja;};
/*!
* \brief Przelicza układ do układu globalnego 
* \return Układ przeliczony do układu globalnego
*/
    Uklad_W Przelicz_uklad_do_globalnego() const;
/*!
* \brief Przelicza wierzchołek(wektor) do układu globalnego
* \param[in]    wierzcholek    wierzcholek(wektor) który ma być przeliczony
* \return wierzcholek (wektor) przeliczony do układu globalnego
*/
    Wektor<3> Przelicz_wierzcholek_do_globalnego(Wektor<3> wierzcholek) const;
/*!
* \brief Konstruktor
* \param[in]    sro   srodek układu
* \param[in]    Orient   orientacja układu
* \param[in]    Rodz   wskaźnik na układu zewnętrzny
*/
    Uklad_W(Wektor<3> Sro, Macierz_Rot<3> Orient, Uklad_W * Rodz): 
    srodek(Sro), orientacja(Orient), rodzic(Rodz) {};
};

/*******************************************************************************/
/*!
* \brief Modeluje pojęcie prostopadlościanu
*
* KLasa modeluje pojęcie prostopadlościanu
*/
class Prostopadloscian : public Interfejs_rysowanie, public Uklad_W {
protected:
/*!
* \brief szerokość prostopadłościanu (wartość na x)
*/
    double szerokosc; 
/*!
* \brief glębokość prostopadłościanu (wartość na y)
*/
    double glebokosc;
/*!
* \brief wysokość prostopadłościanu (wartość na z)
*/
    double wysokosc;
/*!
* \brief id prostopadłościanu generowane podczas rysowania
*/
    int id;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik) const override;
/*!
* \brief udostępnia odległość od środka do wierchołka prostopadłościanu
* Potrzebne do sprawdzania możliwości lądowania
* \return odległość od środka do wierchołka prostopadłościanu
*/
    double get_promien(){return Wektor<3>({szerokosc/2,glebokosc/2,0}).dlugosc();};
/*!
* \brief udostępnia wysokość prostopadłościanu
* Potrzebne do animacji lądowania
* \return wysokość prostopadłościanu
*/
    double get_wysokosc(){return wysokosc;};
/*!
* \brief Kontruktor
* \param[in]    sro   srodek prostopadloscianu
* \param[in]    Orient   orientacja prostopadloscianu
* \param[in]    Rodz   wskaźnik na układu zewnętrzny
* \param[in]    Szerokosc   szerokość prostopadloscianu
* \param[in]    Glebokosc   glębokość prostopadloscianu
* \param[in]    Wysokosc   wysokość prostopadloscianu
*/
    Prostopadloscian(Wektor<3> Sro, Macierz_Rot<3> Orient, Uklad_W * Rodz,
     double Szerokosc, double Glebokosc, double Wysokosc):
    Uklad_W(Sro, Orient, Rodz), szerokosc(Szerokosc), 
    glebokosc(Glebokosc), wysokosc(Wysokosc), id(-1) {};
};

/*******************************************************************************/
/*!
* \brief Modeluje pojęcie graniastosłupa sześciokątnego foremnego
*
* KLasa modeluje pojęcie graniastosłupa sześciokątnego foremnego
*/
class Graniastoslup6: public Interfejs_rysowanie, public Uklad_W {
/*!
* \brief wysokość graniastosłupa
*/
    double wysokosc;
/*!
* \brief długość boku podstawy
*/
    double bok_podst;
/*!
* \brief id generowane podczas rysowania
*/
    int id;
public:
    void rysuj(drawNS::Draw3DAPI* rysownik) override;
    void usun_rys(drawNS::Draw3DAPI* rysownik) const override ;
/*!
* \brief Kontruktor
* \param[in]    sro   srodek graniastosłupa
* \param[in]    Orient   orientacja graniastosłupa
* \param[in]    Rodz   wskaźnik na układu zewnętrzny
* \param[in]    Wys   wysokość graniastosłupa
* \param[in]    Bok   długość boku podstawy
*/
    Graniastoslup6(Wektor<3> Sro, Macierz_Rot<3> Orient, Uklad_W * Rodz, double Wys, double Bok):
    Uklad_W(Sro, Orient, Rodz), wysokosc(Wys), bok_podst(Bok), id(-1) {};
};

/*******************************************************************************/

/*!
* \brief konwertuje Wektor na Point3D (potrzebne do rysowania)
* \param[in]   W    wektor do konwersji
* \return Point3D
*/
drawNS::Point3D konwertuj(Wektor<3> W);

#endif