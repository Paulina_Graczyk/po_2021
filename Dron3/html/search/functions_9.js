var searchData=
[
  ['plaskowyz_176',['Plaskowyz',['../class_plaskowyz.html#ace4ae1899785f131aaa7bbadec09b1b6',1,'Plaskowyz']]],
  ['plaskowyz_5fprost_177',['Plaskowyz_prost',['../class_plaskowyz__prost.html#a2fb001908918b71eff7cb2b0285f0d5c',1,'Plaskowyz_prost']]],
  ['point3d_178',['Point3D',['../classdraw_n_s_1_1_point3_d.html#a0c903a94653375c05122ac5cc73dcf39',1,'drawNS::Point3D::Point3D()=delete'],['../classdraw_n_s_1_1_point3_d.html#a01dac6d46c79850baf2503751974b63b',1,'drawNS::Point3D::Point3D(double x, double y, double z)']]],
  ['pokaz_5fdrony_179',['pokaz_drony',['../class_scena.html#a806c7a3b7a8869ae940e59ebae55f150',1,'Scena']]],
  ['pokaz_5felem_5fkraj_180',['pokaz_elem_kraj',['../class_scena.html#a26fb69a84b94ddc012f948fd25328b66',1,'Scena']]],
  ['powierzchnia_181',['Powierzchnia',['../class_powierzchnia.html#af1a0f3b928d8910cea8c3835e6d196c8',1,'Powierzchnia']]],
  ['prostopadloscian_182',['Prostopadloscian',['../class_prostopadloscian.html#a8a8d96c7235d10482ea7fa3d3ebd6e92',1,'Prostopadloscian']]],
  ['przekrec_5fwirniki_183',['przekrec_wirniki',['../class_dron.html#adce34368d78040533ecf80dab84247b5',1,'Dron::przekrec_wirniki()'],['../class_dron2.html#a008bca2f5a761489ed67218027407e88',1,'Dron2::przekrec_wirniki()'],['../class_dron3.html#a2df732b7f78a35ae1c4b7eec76da8f28',1,'Dron3::przekrec_wirniki()'],['../class_interfejs___dron.html#a86f1013dad83d6ac5b1e019be0a8250f',1,'Interfejs_Dron::przekrec_wirniki()']]],
  ['przelicz_5fuklad_5fdo_5fglobalnego_184',['Przelicz_uklad_do_globalnego',['../class_uklad___w.html#afe1d5e5ff1d0aa1dd2fcbd9273ce4b4d',1,'Uklad_W']]],
  ['przelicz_5fwierzcholek_5fdo_5fglobalnego_185',['Przelicz_wierzcholek_do_globalnego',['../class_uklad___w.html#ac4762243db4657683cf1bfa2ff21440d',1,'Uklad_W']]],
  ['przesuniecie_186',['Przesuniecie',['../class_uklad___w.html#a21f28f16c45f1d2b69ca8df4f2882906',1,'Uklad_W']]]
];
