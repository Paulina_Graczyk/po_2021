#include "Prostopadloscian.hh"

/************************************************************
 * Obraca Prostopadloscian wedlug zadanej macierzy          *
 * Argumenty:                                               *
 *      mac_rot - macierz rotacji                   				*
 ***********************************************************/
  void Prostopadloscian::obrot(Macierz_Rot<3> mac_rot){
      for(int i=0; i<8; i++){
        wierzcholek[i]=mac_rot*wierzcholek[i];
      }
  }

/************************************************************
 * Przesuwa Prostopadloscian o zadany wektor                *
 * Argumenty:                                               *
 *      W - wektor przesuniecia podany w formacie (1,2,3)   *
 ***********************************************************/
  void Prostopadloscian::przesuniecie (Wektor<3> W){
    for(int i=0; i<8; i++){
        wierzcholek[i]=wierzcholek[i]+W;
      }
  }

/************************************************************
 * Porownuje przeciwlegle boki zadanej figury i sprawdza czy *
 * katy sa proste                                           *
 * Zwraca:												                        	*
 *    	true jesli ksztalt spelnia warunki prostokata,      *
 *      false jesli nie         							              *
 ***********************************************************/
  bool Prostopadloscian::czy_bok_to_prostokat(Wektor<3> wsp0, Wektor<3> wsp1, Wektor<3> wsp2, Wektor<3> wsp3) const{
    const double EPSILON = 1E-14;
    Wektor<3> bok_0_1 = wsp0-wsp1;
    Wektor<3> bok_1_2 = wsp1-wsp2;
    Wektor<3> bok_2_3 = wsp2-wsp3;
    Wektor<3> bok_3_0 = wsp3-wsp0;

	if (fabs(bok_0_1.dlugosc()-bok_2_3.dlugosc()) > EPSILON) {
		return false;
	}
	if (fabs(bok_1_2.dlugosc()-bok_3_0.dlugosc()) > EPSILON) {
		return false;
	}
    if ((bok_0_1*bok_1_2)>EPSILON){
        return false;
    }
    if ((bok_1_2*bok_2_3)>EPSILON){
        return false;
    }
    if ((bok_2_3*bok_3_0)>EPSILON){
        return false;
    }
    if ((bok_3_0*bok_0_1)>EPSILON){
        return false;
    }
	return true;
  }

/************************************************************
 * Sprawdza czy podane wierzcholki tworza prostopadloscian  *
 * Sprawdza czy 4 piersze wierzcholki tworza podstawe       *
 * w ksztalcie prostokata i porownuje dlugosci przekatnych  *
 * Zwraca:												                        	*
 *    	true jesli ksztalt spelnia warunki prostopadloscianu*
 *      false jesli nie         							              *
 ***********************************************************/
  bool Prostopadloscian::czyProstopadloscian() const{
    const double EPSILON = 1E-14;
    Wektor<3> przekatna1 = wierzcholek[0]-wierzcholek[6];
    Wektor<3> przekatna2 = wierzcholek[1]-wierzcholek[7];
    Wektor<3> przekatna3 = wierzcholek[2]-wierzcholek[4];
    Wektor<3> przekatna4 = wierzcholek[3]-wierzcholek[5];
    /*sprawdzanie czy podstawa to prostokat*/
    if(!czy_bok_to_prostokat(wierzcholek[0],wierzcholek[1],wierzcholek[2],wierzcholek[3])){
      return false;
    }
    /*sprawdzanie czy przekatne sa sobie rowne*/
    if(przekatna1.dlugosc()-przekatna2.dlugosc()>EPSILON){
      return false;
    }
    if(przekatna2.dlugosc()-przekatna3.dlugosc()>EPSILON){
      return false;
    }
    if(przekatna3.dlugosc()-przekatna4.dlugosc()>EPSILON){
      return false;
    }
    return true;
  }
/*********************************************************************
 * Rysuje Prostopadloscian za pomoca odpowiedniego zlacza do gnuplota*
 * Argumenty:                                                        *
 *      rysownik - wskaznik na element typu Draw2DAPI		             *
 *********************************************************************/
  void Prostopadloscian::rysuj(drawNS::Draw3DAPI* rysownik) {
    id = rysownik->draw_polyhedron(std::vector<std::vector<drawNS::Point3D> > {{
      konwertuj(wierzcholek[0]),konwertuj(wierzcholek[1]),
      konwertuj(wierzcholek[2]),konwertuj(wierzcholek[3])
      },{
      konwertuj(wierzcholek[4]),konwertuj(wierzcholek[5]),
      konwertuj(wierzcholek[6]),konwertuj(wierzcholek[7])
      }},"purple");

  }

/************************************************************
 * Zmazuje narysowany wczesniej Prostopadloscian            *
 * Argumenty:                                               *
*      rysownik - wskaznik na element typu Draw2DAPI	    	*
 ***********************************************************/
    void Prostopadloscian::usun_rys (drawNS::Draw3DAPI* rysownik) const {
        rysownik->erase_shape(id);
    }   

/************************************************************
 * kostruktor dla klasy Prostopadloscian,                   *
 * sprawdza czy zadane wiercholki tworza Prostopadloscian   *
 * Argumenty:                                               *
*      W - tablica wierzcholkow                             *
 ***********************************************************/
  Prostopadloscian::Prostopadloscian (std::array<Wektor<3>, 8> W){

    wierzcholek = W;
      if(czyProstopadloscian() == false){
        std::cerr << "Podane wierzcholki nie tworza prostopadloscianu!" << std::endl;
        exit(2);
      }
  }

/****************************************************************
 * Wypisuje wierzcholki Prostopadloscianu na strumien wyjsciowy	*
 * Argumenty:												                            *
 *    Strm - strumien									                        	*
 *    PR - wyswietlany Prostopadloscian      						        *
 * Zwraca:													                            *
 *    Strumien z zapisem wiercholkow w formie:                  *
 *      (1,2,0)   (4,1,0)                          			        *
 *      (1,0,0)   (4,0,0)                         			        *
 *      (1,2,2)   (4,1,2)                         			        *
 *      (1,0,2)   (4,0,2)                         			        *
 ****************************************************************/
  std::ostream& operator << ( std::ostream &Strm,const Prostopadloscian&Pr){
      Strm << Pr[0] << "    " << Pr[1] << std::endl;
      Strm << Pr[2] << "    " << Pr[3] << std::endl; 
      Strm << Pr[4] << "    " << Pr[5] << std::endl;
      Strm << Pr[6] << "    " << Pr[7] << std::endl;
    return Strm;
  }


/************************************************************
 * Konwertuje wektor na Point2D                             *
 * Argumenty:                                               *
 *      W - wektor do przekonwertowania    			        		*
 * Zwraca:											                        		*
 *    	Przekonwertowany punkt    	            						*
 ***********************************************************/
  drawNS::Point3D konwertuj(Wektor<3> W){
      return drawNS::Point3D(W[0],W[1],W[2]);
}