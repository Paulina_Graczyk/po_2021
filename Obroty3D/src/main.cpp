#include <iostream>
#include <iomanip>
#include <fstream>
#include "Wektor.hh"
#include "Macierz.hh"
#include "Prostopadloscian.hh"

/************************************************************
 * Funkcja wyswietla menu na standardowe wyjscie            *
 ***********************************************************/
void wyswietl_menu(){
    std::cout << "Dostepne opcje: " << std::endl;
    std::cout << "**************************************" << std::endl;
    std::cout << "o - obrot prostopadloscianu o zadana sekwencje katow" << std::endl;
    std::cout << "t - powtorzenie poprzedniego obrotu" << std::endl;
    std::cout << "r - wyswietl macierz rotacji" << std::endl;
    std::cout << "p - przesuniecie prostopadloscianu o wektor" << std::endl;
    std::cout << "w - wyswietl wspolrzedne wierzcholkow" << std::endl;
    std::cout << "m - wyswietl menu" << std::endl;
    std::cout << "k - zakoncz program  " << std::endl;
}

int main(){ 
char wybor;
char os_char_tmp;
double kat_tmp;
std::vector<Wektor<3>> kolej_osie ;
std::vector<double> kolej_katy ;
int obroty;
Macierz_Rot<3> mac_obr_poprzednia;
Wektor<3> wek_przes;

/*wektory podstawowych osi*/
Wektor<3> wekx({1,0,0}); 
Wektor<3> weky({0,1,0}); 
Wektor<3> wekz({0,0,1}); 

/*Wierzcholki prostopadloscianu*/
Wektor<3> wek1({0.0,0.0,0.0});
Wektor<3> wek2({3.0,0.0,0.0});
Wektor<3> wek3({3.0,5.0,0.0});
Wektor<3> wek4({0.0,5.0,0.0});
Wektor<3> wek5({0.0,0.0,5.0});
Wektor<3> wek6({3.0,0.0,5.0});
Wektor<3> wek7({3.0,5.0,5.0});
Wektor<3> wek8({0.0,5.0,5.0});
/*Tworzenie prostopadloscianu o zadanych wierzcholkach*/
Prostopadloscian Prostopad_1(std::array<Wektor<3>,8>{wek1,wek2,wek3,wek4,wek5,wek6,wek7,wek8});

drawNS::Draw3DAPI * rysownik = new drawNS::APIGnuPlot3D(-10,10,-10,10,-10,10,0);

/*Ustawienie ilosci wyswietlanych miejsc po przecinku*/
std::cout << std::setprecision(15)<<std::fixed;

std::cout << "Twoj prostopadlocian to:" << std::endl;
std::cout << Prostopad_1;
Prostopad_1.rysuj(rysownik);
std::cout << std::endl;

wyswietl_menu();
while (wybor!= 'k'){
    Macierz_Rot<3> mac_skl; 
    std::cout << "Twoj wybor: (m - menu) > ";
    std::cin >> wybor;
    switch(wybor){
    case 'o' : //obrot o kat
        Prostopad_1.usun_rys(rysownik); //zmaz poprzednio narysowany prostokat
        std::cin.clear();

        std::cout << "Podaj sekwencje oznaczen osi i katy obrotu w stopniach: " << std::endl;
        std::cout << "Dozwolone znaki to: . x y z "<< std::endl;
        /*wczytywanie sekwencji osi i katow do kontenerow*/
        std::cin >> os_char_tmp;
        while(os_char_tmp!='.'){
            switch(os_char_tmp){
                case 'x':
                kolej_osie.push_back(wekx); 
                std::cin >> kat_tmp;
                kolej_katy.push_back(kat_tmp);break;
                case 'y':
                kolej_osie.push_back(weky); 
                std::cin >> kat_tmp;
                kolej_katy.push_back(kat_tmp);break;
                case 'z':
                kolej_osie.push_back(wekz); 
                std::cin >> kat_tmp;
                kolej_katy.push_back(kat_tmp);break;
                default:
                std::cerr << "Nie ma takiej osi" << std::endl;
                std::cerr << "Sprobuj ponownie: " << std::endl;
                std::cin.clear();
                std::cin.ignore(10000, '\n');
            }
            
            std::cin >> os_char_tmp;
        }

        std::cout << "Ile razy chcesz obrocic? > ";
        std::cin >> obroty;

        /*skladanie macierzy obrotow*/
        while(!kolej_katy.empty()){
            Macierz_Rot<3> mac_tmp(kolej_katy.back(), kolej_osie.back());
            kolej_katy.pop_back();
            kolej_osie.pop_back();
            mac_skl = mac_tmp*mac_skl;
        }
        /*powtorz obrot zadana ilosc razy*/
        for(int i=0; i<obroty; i++){
            Prostopad_1.obrot(mac_skl);
        }

        mac_obr_poprzednia = mac_skl; //ustaw nowa poprzednia macierz obrotu

        Prostopad_1.rysuj(rysownik); 

        if(Prostopad_1.czyProstopadloscian()){
            std::cout << "Boki i katy nadal sie zgadzaja :)" << std::endl;
        } else {
            std::cout << "Boki i katy przestaly sie zgadzac :(" <<std::endl;
            std::cout << "Pierwsza para przeciwleglych bokow:  " <<std::endl;
            std::cout << (Prostopad_1[0]- Prostopad_1[1]).dlugosc() << "    " << (Prostopad_1[7]-Prostopad_1[6]).dlugosc() << std::endl;
            std::cout << "Druda para przeciwleglych bokow: " <<std::endl;
            std::cout << (Prostopad_1[1]- Prostopad_1[2]).dlugosc() << "    " << (Prostopad_1[4]-Prostopad_1[7]).dlugosc() << std::endl;
            std::cout << "Trzecia para przeciwleglych bokow: " <<std::endl;
            std::cout << (Prostopad_1[1]- Prostopad_1[5]).dlugosc() << "    " << (Prostopad_1[3]-Prostopad_1[7]).dlugosc() << std::endl;
        
        }
        break;
    case 't' :  // powtorzenie poprzedniego obrotu
        Prostopad_1.usun_rys(rysownik); 
        Prostopad_1.obrot(mac_obr_poprzednia);
        Prostopad_1.rysuj(rysownik);
        if(Prostopad_1.czyProstopadloscian()){
            std::cout << "Boki i katy nadal sie zgadzaja :)" << std::endl;
        } else {
            std::cout << "Boki i katy przestaly sie zgadzac :(" <<std::endl;
            std::cout << "Pierwsza para przeciwleglych bokow:  " <<std::endl;
            std::cout << (Prostopad_1[0]- Prostopad_1[1]).dlugosc() << "    " << (Prostopad_1[7]-Prostopad_1[6]).dlugosc() << std::endl;
            std::cout << "Druda para przeciwleglych bokow: " <<std::endl;
            std::cout << (Prostopad_1[1]- Prostopad_1[2]).dlugosc() << "    " << (Prostopad_1[4]-Prostopad_1[7]).dlugosc() << std::endl;
            std::cout << "Trzecia para przeciwleglych bokow: " <<std::endl;
            std::cout << (Prostopad_1[1]- Prostopad_1[5]).dlugosc() << "    " << (Prostopad_1[3]-Prostopad_1[7]).dlugosc() << std::endl;
        
        }
        break;
    case 'r'  : // wyswietlanie macierzy obrotu
        std::cout << mac_obr_poprzednia << std::endl;
        break;
    case 'p' :  //przesuniecie o wektor
        Prostopad_1.usun_rys(rysownik); //zmaz poprzednio narysowany prostopadloscian
        std::cout << "Podaj wektor przesuniecia: (poprawna forma to (1,2,3) ) > ";
        while(!(std::cin >> wek_przes)){    //sprawdzenie poprawnosci wpisania wektora
            std::cerr << "Nie poprawnie wpisany wektor!"<< std::endl;
            std::cout << "Sprobuj jeszcze raz: ";
            std::cin.clear();
            std::cin.ignore(10000, '\n');
        }
        Prostopad_1.przesuniecie(wek_przes);
        Prostopad_1.rysuj(rysownik);
        break;
    case 'w' :  //wyswietlenie wierzcholkow
        std::cout << Prostopad_1;
        break;
    case 'm' :  //wyswielt menu
        wyswietl_menu();
        break;
    case 'k' : //zakoncz program
        break;
    default : 
        std::cerr << "Brak takiej opcji!" << std::endl;
        std::cout << "Sprobuj jeszcze raz: " << std::endl;
    }
}

delete rysownik;
exit(0);    // zakoncz program pomyslnie
}

